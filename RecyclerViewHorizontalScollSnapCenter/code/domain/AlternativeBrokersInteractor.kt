package com.hidden.android.alternative_brokers.domain

import androidx.paging.PagedList
import com.hidden.android.alternative_brokers.di.DIConstants
import com.hidden.android.base_shared_model.error.CatException
import com.hidden.android.entities.brokers.AlternativeBroker
import com.hidden.android.gateway.BrokersGateway
import com.hidden.android.gateway.paging.LiveListing
import com.hidden.android.shared_interactor.base.Interactor0
import javax.inject.Inject
import javax.inject.Named

class AlternativeBrokersInteractor @Inject constructor(
    private val brokersGateway: BrokersGateway,
    @Named(DIConstants.DI_NAME_ALTERNATIVE_BROKERS_PAGE_CONFIG) private val pagedListConfig: PagedList.Config
) : Interactor0<LiveListing<AlternativeBroker, CatException>>() {

    override fun get(args: Unit): LiveListing<AlternativeBroker, CatException> {
        return brokersGateway.fetchAlternativeBrokersPaged(pagedListConfig = pagedListConfig)
    }
}