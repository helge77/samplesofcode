package com.hidden.android.alternative_brokers.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.PagerSnapHelper
import com.hidden.android.alternative_brokers.R
import com.hidden.android.alternative_brokers.databinding.FragmentAlternativeBrokersBinding
import com.hidden.android.alternative_brokers.ui.adapter.AlternativeBrokerItemListener
import com.hidden.android.alternative_brokers.ui.adapter.AlternativeBrokersAdapter
import com.hidden.android.alternative_brokers.ui.view.OnSnapPositionChangeListener
import com.hidden.android.alternative_brokers.ui.view.attachSnapHelperWithListener
import com.hidden.android.base_shared_model.error.CatException
import com.hidden.android.base_shared_model.resource.Resource
import com.hidden.android.base_shared_model.resource.Status
import com.hidden.android.common.di.qualifiers.ActivityContext
import com.hidden.android.entities.brokers.AlternativeBroker
import com.hidden.android.shared_ui.BaseViewModelFragment
import com.hidden.android.shared_ui.UIConstants
import com.hidden.android.shared_ui.tag.HasFragmentTag
import com.hidden.android.shared_ui.tag.SimpleFragmentTagGenerator
import com.hidden.android.shared_ui.view.CenterSliderScaleAlphaLayoutManager
import com.hidden.android.shared_ui.view.GeneralSnackBarHelper
import com.hidden.android.shared_ui.view.toolbar.ToolbarStartButtonClickListener
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.swiperefreshlayout.refreshes
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AlternativeBrokersFragment : BaseViewModelFragment<AlternativeBrokersViewModel>(),
    HasFragmentTag {

    @JvmField
    internal var listener: InteractionListener? = null

    @Inject
    @ActivityContext
    internal lateinit var activityContext: Context

    @Inject
    internal lateinit var alternativeBrokersAdapter: AlternativeBrokersAdapter

    private lateinit var binding: FragmentAlternativeBrokersBinding

    private lateinit var uiDisposables: CompositeDisposable

    private var errorMessageWithRetry: Snackbar? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is InteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement ${InteractionListener::class.java}")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        uiDisposables = CompositeDisposable()
        binding = FragmentAlternativeBrokersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()

        setupRecycleView()

        assignListeners(uiDisposables)
    }

    override fun onAttachViewModel(savedInstanceState: Bundle?) {
        super.onAttachViewModel(savedInstanceState)

        viewModel.networkState.observe(viewLifecycleOwner, networkStateAlternativeBrokerListObserver)
        viewModel.alternativeBrokersPagedList.observe(
            viewLifecycleOwner, fetchAlternativeBrokerPageListObserver
        )
        viewModel.refreshState.observe(viewLifecycleOwner, refreshAlternativeBrokerListObserver)

        if (viewModel.alternativeBrokersPagedList.value == null) {
            viewModel.fetchAlternativeBrokers()
            viewModel.refreshAlternativeBrokers()
        }

        viewModel.connectAlternativeBrokerLiveData.observe(
            viewLifecycleOwner, connectAlternativeBrokerObserver
        )
    }

    override fun onDetachViewModel() {
        viewModel.refreshState.removeObservers(viewLifecycleOwner)
        viewModel.alternativeBrokersPagedList.removeObservers(viewLifecycleOwner)
        viewModel.networkState.removeObservers(viewLifecycleOwner)
        viewModel.connectAlternativeBrokerLiveData.removeObservers(viewLifecycleOwner)

        super.onDetachViewModel()
    }

    override fun onDestroyView() {
        uiDisposables.dispose()
        binding.alternativeBrokerListRv.adapter = null

        super.onDestroyView()
    }

    private fun setupRecycleView() {
        binding.alternativeBrokerListRv.apply {
            adapter = alternativeBrokersAdapter

            setHasFixedSize(true)

            val layoutManager = CenterSliderScaleAlphaLayoutManager(
                activityContext,
                resources.getDimension(R.dimen.alternative_broker_item_content_margin).toInt()
            )

            this.layoutManager = layoutManager

            attachSnapHelperWithListener(PagerSnapHelper(), object : OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    viewModel.selectedBrokerPosition = position
                    updateButtonAndDescription(position)
                }
            })

            binding.alternativeBrokersPageIndicator.attachToRecyclerView(this)
        }
    }

    private fun updateButtonAndDescription(position: Int) {
        viewModel.alternativeBrokersPagedList.value?.let {
            if (position < it.size) {
                val broker = it[position]
                binding.alternativeBrokersConnectBtn.tag = broker?.brandId
                binding.isActive = broker?.isActive ?: false
            }
        }
    }

    private fun assignListeners(uiDisposable: CompositeDisposable) {
        uiDisposable.addAll(
            binding.alternativeBrokersToolbar.tbTopStartButton
                .clicks()
                .throttleFirst(
                    UIConstants.VIEW_CLICKS_THROTTLE_MILLIS,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread()
                )
                .subscribe { listener?.onStartButtonClick() },
            binding.alternativeBrokersRefreshLayout
                .refreshes()
                .subscribe {
                    viewModel.refreshAlternativeBrokers()
                },
            binding.alternativeBrokersToolbar.tbTopEndButton
                .clicks()
                .throttleFirst(
                    UIConstants.VIEW_CLICKS_THROTTLE_MILLIS,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread()
                )
                .subscribe {
                    if (viewModel.networkState.value?.let { Resource.loading(it) } != true) {
                        viewModel.refreshAlternativeBrokers()
                    }
                },
            binding.alternativeBrokersConnectBtn
                .clicks()
                .throttleFirst(
                    UIConstants.VIEW_CLICKS_THROTTLE_MILLIS,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread()
                )
                .subscribe {
                    binding.alternativeBrokersConnectBtn.tag?.let {
                        val brandId = it as Long
                        viewModel.connectAlternativeBroker(brandId = brandId)
                    }
                }
        )
    }

    private val fetchAlternativeBrokerPageListObserver =
        Observer<PagedList<AlternativeBroker>> { alternativeBrokers ->
            dismissErrorMessageWithRetry()
            alternativeBrokersAdapter.submitList(alternativeBrokers) {
                (binding.alternativeBrokerListRv.layoutManager as CenterSliderScaleAlphaLayoutManager).updateChildrenScale()
            }
            binding.showPlaceholder = alternativeBrokers.isEmpty()
            updateButtonAndDescription(viewModel.selectedBrokerPosition)
        }

    private val refreshAlternativeBrokerListObserver = Observer<Resource<Unit, CatException>> { r ->
        if (Resource.failed(r)) {
            showErrorMessage(requireNotNull(r.error))
        }
    }

    private val networkStateAlternativeBrokerListObserver =
        Observer<Resource<Unit, CatException>> { r ->
            binding.isLoading = Resource.loading(r)
            if (Resource.failed(r)) {
                showErrorMessageWithRetry(requireNotNull(r.error))
            }
        }

    private val connectAlternativeBrokerObserver: Observer<Resource<Unit, CatException>> =
        Observer {

            binding.isConnecting = Resource.loading(it)

            when (it.status) {
                Status.INITIAL -> {
                }

                Status.SUCCESS -> {
                    setFragmentResult(UIConstants.FRAGMENT_RESULT_KEY_BROKERS_UPDATED, bundleOf())
                }

                Status.ERROR -> {
                    val error = requireNotNull(it.error)
                    Timber.e(error)
                    showErrorMessage(error)
                }

                Status.LOADING -> {
                }
            }
        }

    private fun showErrorMessage(error: CatException) {
        GeneralSnackBarHelper.getGeneralMessage(
            errorMessage = error.message ?: getString(R.string.network_error_default),
            rootView = binding.root,
            duration = Snackbar.LENGTH_SHORT
        ).show()
    }

    private fun showErrorMessageWithRetry(error: CatException) {
        errorMessageWithRetry = GeneralSnackBarHelper.showMessageWithRetry(
            errorMessage = error.message ?: getString(R.string.network_error_default),
            rootView = binding.root,
            listener = View.OnClickListener {
                viewModel.retryAlternativeBrokersFailedPage()
            })
        errorMessageWithRetry?.show()
    }

    private fun dismissErrorMessageWithRetry() {
        errorMessageWithRetry?.dismiss()
        errorMessageWithRetry = null
    }

    private fun initToolbar() {
        binding.alternativeBrokersToolbar.apply {
            title = getString(R.string.alternative_brokers_toolbar_text)
            endBtnImageRes = R.drawable.ic_update
        }
    }

    interface InteractionListener : ToolbarStartButtonClickListener

    override fun getViewModelClass(): Class<AlternativeBrokersViewModel> =
        AlternativeBrokersViewModel::class.java

    override fun getFragmentTag(): String = FRAGMENT_TAG

    val alternativeBrokerItemListener: AlternativeBrokerItemListener =
        object : AlternativeBrokerItemListener {

            override fun onUpgradeBtnClick(alternativeBroker: AlternativeBroker) {
                Timber.d("${alternativeBroker.brandId}")
            }
        }

    override fun getScreenName(): String = "Alternative Brokers"

    companion object {

        val FRAGMENT_TAG =
            SimpleFragmentTagGenerator.generate(AlternativeBrokersFragment::class.java)

        val ALTERNATIVE_BROKER_DIFF_CALLBACK: DiffUtil.ItemCallback<AlternativeBroker> =
            object : DiffUtil.ItemCallback<AlternativeBroker>() {

                override fun areItemsTheSame(
                    oldItem: AlternativeBroker,
                    newItem: AlternativeBroker
                ): Boolean {
                    return oldItem.brandId == newItem.brandId
                }

                override fun areContentsTheSame(
                    oldItem: AlternativeBroker,
                    newItem: AlternativeBroker
                ): Boolean {
                    return oldItem == newItem
                }
            }

        @JvmStatic
        fun newInstance(): AlternativeBrokersFragment = AlternativeBrokersFragment()
    }
}