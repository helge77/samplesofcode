package com.hidden.android.alternative_brokers.domain

import com.hidden.android.base_shared_model.error.CatException
import com.hidden.android.base_shared_model.resource.Resource
import com.hidden.android.gateway.BrokersGateway
import com.hidden.android.shared_interactor.base.ObservableInteractor
import com.hidden.android.shared_interactor.utils.RxInteractorUtils
import io.reactivex.Observable
import javax.inject.Inject

class ConnectAlternativeBrokerInteractor @Inject constructor(
    private val brokersGateway: BrokersGateway
) : ObservableInteractor<Resource<Unit, CatException>, Long>() {

    override fun get(args: Long): Observable<Resource<Unit, CatException>> {
        return Observable.concat(
            RxInteractorUtils.loadingSource<Unit, CatException>(null).toObservable(),
            brokersGateway
                .connectAlternativeBroker(brandId = args)
                .map {
                    Resource.success(
                        it,
                        null as CatException?
                    )
                }
                .onErrorReturn { th: Throwable -> Resource.error(null, th as CatException) }
                .toObservable()
        )
    }
}