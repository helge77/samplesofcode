package com.hidden.android.alternative_brokers.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.hidden.android.alternative_brokers.databinding.LayoutAlternativeBrokerItemBinding
import com.hidden.android.entities.brokers.AlternativeBroker
import com.hidden.android.shared_ui.view.BindingViewHolder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class AlternativeBrokerItemViewHolder constructor(
    binding: LayoutAlternativeBrokerItemBinding,
    listener: AlternativeBrokerItemListener
) : BindingViewHolder<AlternativeBroker, LayoutAlternativeBrokerItemBinding>(binding) {

    private val holderDisposable = CompositeDisposable()

    init {
        assignListeners(listener)
    }

    fun getDisposable() = holderDisposable as Disposable

    override fun bind(d: AlternativeBroker) {
        itemView.tag = d
        binding.alternativeBroker = d
        binding.executePendingBindings()
    }

    private fun assignListeners(listener: AlternativeBrokerItemListener) {

    }

    companion object {
        fun create(
            inflater: LayoutInflater,
            parent: ViewGroup,
            alternativeBrokerItemListener: AlternativeBrokerItemListener
        ): AlternativeBrokerItemViewHolder {
            return AlternativeBrokerItemViewHolder(
                LayoutAlternativeBrokerItemBinding.inflate(inflater, parent, false),
                alternativeBrokerItemListener
            )
        }
    }
}