package com.hidden.android.alternative_brokers.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hidden.android.entities.brokers.AlternativeBroker
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AlternativeBrokersAdapter @Inject constructor(
    diffCallback: DiffUtil.ItemCallback<AlternativeBroker>,
    private var listener: AlternativeBrokerItemListener
) : PagedListAdapter<AlternativeBroker, AlternativeBrokerItemViewHolder>(diffCallback) {

    private val disposables = CompositeDisposable()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AlternativeBrokerItemViewHolder {
        val vh = AlternativeBrokerItemViewHolder.create(
            LayoutInflater.from(parent.context),
            parent,
            listener
        )
        disposables.add(vh.getDisposable())
        return vh
    }

    override fun onBindViewHolder(holder: AlternativeBrokerItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)

        disposables.clear()
    }
}