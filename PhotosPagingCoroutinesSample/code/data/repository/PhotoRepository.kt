package com.arch.example.data.repository

import androidx.paging.PagingData
import com.arch.example.data.models.photo.Photo
import kotlinx.coroutines.flow.Flow

interface PhotoRepository {

    fun getPhotos(): Flow<PagingData<Photo>>
}