package com.arch.example.util

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.arch.example.R
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.io.File

object BindingAdapters {

    @BindingAdapter("imageUrl", "placeholderResId", "placeHolderColor", "errorResId", requireAll = false)
    @JvmStatic
    fun imageUrl(
        imageView: ImageView,
        url: String?,
        @DrawableRes placeholderResId: Int?,
        placeHolderColor: String?,
        @DrawableRes errorResId: Int?
    ) {
        if (!url.isNullOrEmpty()) {
            loadImage(
                imageView, setPlaceholder(
                    placeholderResId, placeHolderColor, Glide.with(imageView.context)
                        .load(url)
                        .error(errorResId ?: R.drawable.ic_placeholder)
                )
            )
        }
    }

    @BindingAdapter("imageFile")
    @JvmStatic
    fun imageFile(imageView: ImageView, file: File?) {
        file?.let { loadImage(imageView, Glide.with(imageView.context).load(it)) }
    }

    @BindingAdapter("imageResId")
    @JvmStatic
    fun imageResId(imageView: ImageView, @DrawableRes id: Int?) {
        id?.let { Glide.with(imageView).load(it).into(imageView) }
    }

    private fun setPlaceholder(
        @DrawableRes placeholderResId: Int?,
        placeHolderColor: String?,
        request: RequestBuilder<Drawable>
    ): RequestBuilder<Drawable> {
        return placeHolderColor?.let {
            request.placeholder(ColorDrawable(Color.parseColor(placeHolderColor)))
        } ?: run {
            request.placeholder(placeholderResId ?: R.drawable.ic_placeholder)
        }
    }

    private fun loadImage(imageView: ImageView, request: RequestBuilder<Drawable>) {
        request.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(imageView)
    }
}