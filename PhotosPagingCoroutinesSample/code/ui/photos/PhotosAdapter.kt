package com.arch.example.ui.photos

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.arch.example.base.view.BaseViewHolder
import com.arch.example.data.models.photo.Photo
import com.arch.example.databinding.LayoutItemPhotoBinding

class PhotosAdapter(
    private val onPhotoClicked: (photo: Photo) -> Unit
) : PagingDataAdapter<Photo, PhotosAdapter.PhotoViewHolder>(PhotoComparator) {

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PhotoViewHolder(
            LayoutItemPhotoBinding.inflate(layoutInflater, parent, false),
            onPhotoClicked
        )
    }

    inner class PhotoViewHolder(
        binding: LayoutItemPhotoBinding,
        val onPhotoClicked: (photo: Photo) -> Unit
    ) : BaseViewHolder<LayoutItemPhotoBinding, Photo>(binding) {

        init {
            binding.holder = this
        }

        override fun bind(data: Photo, position: Int) {
            binding.photo = data
        }
    }

    private object PhotoComparator : DiffUtil.ItemCallback<Photo>() {
        override fun areItemsTheSame(oldItem: Photo, newItem: Photo) = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Photo, newItem: Photo) = oldItem == newItem
    }
}