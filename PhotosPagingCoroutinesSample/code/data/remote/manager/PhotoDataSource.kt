package com.arch.example.data.remote.manager

import com.arch.example.data.models.photo.Photo

interface PhotoDataSource {

    suspend fun getPhotos(page: Int, perPage: Int): List<Photo>
}