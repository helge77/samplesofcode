package com.hidden.android.alternative_brokers.di

import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.hidden.android.alternative_brokers.ui.AlternativeBrokersFragment
import com.hidden.android.alternative_brokers.ui.AlternativeBrokersViewModel
import com.hidden.android.alternative_brokers.ui.adapter.AlternativeBrokerItemListener
import com.hidden.android.alternative_brokers.ui.adapter.AlternativeBrokersAdapter
import com.hidden.android.common.di.keys.ViewModelKey
import com.hidden.android.entities.brokers.AlternativeBroker
import com.hidden.android.shared_ui.BaseViewModelFragment
import com.hidden.android.shared_ui.di.BaseInjectableFragmentModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Named

@Module(includes = [BaseInjectableFragmentModule::class])
interface AlternativeBrokersFragmentModule {

    @Binds
    fun bindAlternativeBrokersFragment(
        fragment: AlternativeBrokersFragment
    ): BaseViewModelFragment<AlternativeBrokersViewModel>

    @Binds
    @IntoMap
    @ViewModelKey(AlternativeBrokersViewModel::class)
    fun bindAlternativeBrokersViewModel(viewModel: AlternativeBrokersViewModel): ViewModel

    companion object {
        const val PAGE_SIZE = 35
        const val PREFETCH_DISTANCE = 10
        const val PLACEHOLDERS_ENABLED = false

        @Named(DIConstants.DI_NAME_ALTERNATIVE_BROKERS_PAGE_CONFIG)
        @Provides
        fun providePagedListConfig(): PagedList.Config {
            return PagedList.Config
                .Builder()
                .setPageSize(PAGE_SIZE)
                .setPrefetchDistance(PREFETCH_DISTANCE)
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setEnablePlaceholders(PLACEHOLDERS_ENABLED)
                .build()
        }

        @Provides
        fun provideAlternativeBrokerItemDiffCallback(): DiffUtil.ItemCallback<AlternativeBroker> {
            return AlternativeBrokersFragment.ALTERNATIVE_BROKER_DIFF_CALLBACK
        }

        @Provides
        fun provideAlternativeBrokerItemListener(
            fragment: AlternativeBrokersFragment
        ): AlternativeBrokerItemListener {
            return fragment.alternativeBrokerItemListener
        }

        @Provides
        fun provideAlternativeBrokerItemAdapter(
            diffCallback: DiffUtil.ItemCallback<AlternativeBroker>,
            listener: AlternativeBrokerItemListener
        ): AlternativeBrokersAdapter {
            return AlternativeBrokersAdapter(
                diffCallback = diffCallback,
                listener = listener
            )
        }
    }
}