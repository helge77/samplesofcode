package com.arch.example.ui.photos

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.arch.example.R
import com.arch.example.base.BaseFragmentViewModel
import com.arch.example.base.view.LoadingStateAdapter
import com.arch.example.data.Result
import com.arch.example.databinding.FragmentPhotosBinding
import com.arch.example.util.toResult
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PhotosFragment : BaseFragmentViewModel<FragmentPhotosBinding, PhotosViewModel>() {

    override val viewModel: PhotosViewModel by viewModels()

    override fun getFragmentLayoutRedId(): Int = R.layout.fragment_photos

    private lateinit var photosAdapter: PhotosAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewModel = viewModel

        setupPhotosRecyclerView()
        initListeners()
        initObservers()

        if (viewModel.photosResponse.value == null) {
            viewModel.loadPhotos()
        }
    }

    private fun setupPhotosRecyclerView() {
        photosAdapter = PhotosAdapter { photo ->
            // TODO: open photo viewer
        }
        binding.photosRecyclerView.adapter = photosAdapter.let {
            it.withLoadStateHeaderAndFooter(
                header = LoadingStateAdapter { it.retry() },
                footer = LoadingStateAdapter { it.retry() }
            )
        }
    }

    private fun initListeners() {
        binding.photosSwipeRefresh.setOnRefreshListener {
            photosAdapter.refresh()
        }
    }

    private fun initObservers() {
        viewModel.photosResponse.observe(viewLifecycleOwner) { pagingData ->
            photosAdapter.submitData(viewLifecycleOwner.lifecycle, pagingData)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            photosAdapter.loadStateFlow.map { it.refresh.toResult() }
                .collect { result ->
                    if (result is Result.Error) {
                        showSnackBarWithAction(result.message, Snackbar.LENGTH_INDEFINITE) {
                            photosAdapter.refresh()
                        }
                    } else {
                        dismissSnackBar()
                    }
                    viewModel.updatePhotosLoadStatus(result)
                    viewModel.updatePhotosEmptyStatus(photosAdapter.itemCount == 0)
                }
        }
    }
}