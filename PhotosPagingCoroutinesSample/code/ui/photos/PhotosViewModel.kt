package com.arch.example.ui.photos

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.arch.example.data.Result
import com.arch.example.data.models.photo.Photo
import com.arch.example.data.repository.PhotoRepository
import com.arch.example.util.isActive
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PhotosViewModel @Inject constructor(
    private val photoRepository: PhotoRepository
) : ViewModel() {

    private var jobFetchPhotos: Job? = null
    private val _photosResponse = MutableLiveData<PagingData<Photo>>()
    val photosResponse: LiveData<PagingData<Photo>> = _photosResponse

    private val _photosLoadStatus = MutableLiveData<Result<Unit>>(Result.Initial)
    val photosLoadStatus: LiveData<Result<Unit>> = _photosLoadStatus.distinctUntilChanged()

    private val _photosEmpty = MutableLiveData<Boolean>()
    val photosEmpty: LiveData<Boolean> = _photosEmpty.distinctUntilChanged()

    fun loadPhotos() {
        if (jobFetchPhotos.isActive) return
        jobFetchPhotos = viewModelScope.launch {
            photoRepository.getPhotos()
                .cachedIn(viewModelScope)
                .collect {
                    _photosResponse.value = it
                }
        }
    }

    fun updatePhotosLoadStatus(photosLoadStatus: Result<Unit>) {
        this._photosLoadStatus.value = photosLoadStatus
    }

    fun updatePhotosEmptyStatus(isEmpty: Boolean) {
        this._photosEmpty.value = isEmpty
    }
}