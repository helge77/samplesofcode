package com.hidden.android.alternative_brokers.ui.view

interface OnSnapPositionChangeListener {
    fun onSnapPositionChange(position: Int)
}