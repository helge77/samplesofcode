package com.arch.example.data.remote.manager.implementation

import com.arch.example.data.models.photo.Photo
import com.arch.example.data.remote.api.PhotoApi
import com.arch.example.data.remote.manager.PhotoDataSource
import com.arch.example.data.remote.models.photo.NetworkPhoto
import com.arch.example.data.remote.utils.NetworkErrorConverterHelper

class PhotoDataSourceImpl(
    private val photoApi: PhotoApi,
    private val networkErrorConverterHelper: NetworkErrorConverterHelper
) : PhotoDataSource {

    override suspend fun getPhotos(page: Int, perPage: Int): List<Photo> = try {
        val data = photoApi.getPhotos(page, perPage)
        data.map { NetworkPhoto.mapToModel(it) }
    } catch (e: Throwable) {
        throw networkErrorConverterHelper.parseError(e)
    }
}