package com.hidden.android.alternative_brokers.ui.adapter

import com.hidden.android.entities.brokers.AlternativeBroker

interface AlternativeBrokerItemListener {

    fun onUpgradeBtnClick(alternativeBroker: AlternativeBroker)

}