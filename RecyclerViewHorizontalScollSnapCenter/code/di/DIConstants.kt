package com.hidden.android.alternative_brokers.di

internal class DIConstants private constructor() {
    companion object {
        const val DI_NAME_ALTERNATIVE_BROKERS_PAGE_CONFIG = "paged_list_config_alternative_brokers"
    }
}