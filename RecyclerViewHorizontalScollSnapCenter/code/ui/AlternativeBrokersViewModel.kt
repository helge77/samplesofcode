package com.hidden.android.alternative_brokers.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.hidden.android.alternative_brokers.domain.AlternativeBrokersInteractor
import com.hidden.android.alternative_brokers.domain.ConnectAlternativeBrokerInteractor
import com.hidden.android.base_shared_model.error.CatException
import com.hidden.android.base_shared_model.error.ErrorCategory
import com.hidden.android.base_shared_model.resource.Resource
import com.hidden.android.entities.brokers.AlternativeBroker
import com.hidden.android.gateway.paging.LiveListing
import com.hidden.android.shared_interactor.domain.ImpressionLogsInteractor
import com.hidden.android.shared_ui.BaseViewModelScreenName
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import timber.log.Timber
import javax.inject.Inject

class AlternativeBrokersViewModel @Inject constructor(
    private val alternativeBrokersInteractor: AlternativeBrokersInteractor,
    private val connectAlternativeBrokerInteractor: ConnectAlternativeBrokerInteractor,
    impressionLogsInteractor: ImpressionLogsInteractor
) : BaseViewModelScreenName(impressionLogsInteractor) {

    private val alternativeBrokersLiveListingLiveData =
        MutableLiveData<LiveListing<AlternativeBroker, CatException>>()

    private val connectAlternativeBrokerLiveDataMutable =
        MutableLiveData<Resource<Unit, CatException>>()

    private var connectAlternativeBrokerDisposable = Disposables.disposed()

    var selectedBrokerPosition: Int = 0

    val alternativeBrokersPagedList =
        Transformations.switchMap(alternativeBrokersLiveListingLiveData) { it.pagedList }
    val networkState =
        Transformations.switchMap(alternativeBrokersLiveListingLiveData) { it.networkState }
    val refreshState =
        Transformations.switchMap(alternativeBrokersLiveListingLiveData) { it.refreshState }

    val connectAlternativeBrokerLiveData: LiveData<Resource<Unit, CatException>> =
        connectAlternativeBrokerLiveDataMutable

    fun fetchAlternativeBrokers() {
        alternativeBrokersLiveListingLiveData.value = alternativeBrokersInteractor.get(Unit)
    }

    fun refreshAlternativeBrokers() {
        alternativeBrokersLiveListingLiveData.value?.refresh?.invoke()
    }

    fun retryAlternativeBrokersFailedPage() {
        alternativeBrokersLiveListingLiveData.value?.retry?.invoke()
    }

    fun connectAlternativeBroker(
        brandId: Long
    ) {
        connectAlternativeBrokerDisposable.dispose()
        connectAlternativeBrokerDisposable = connectAlternativeBrokerInteractor
            .get(brandId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { success -> connectAlternativeBrokerLiveDataMutable.value = success },
                { th ->
                    Timber.e(th)
                    connectAlternativeBrokerLiveDataMutable.value = Resource.error(
                        null,
                        CatException(ErrorCategory.UNSPECIFIED, th.message, th)
                    )
                }
            )
    }

    override fun onCleared() {
        super.onCleared()

        connectAlternativeBrokerDisposable.dispose()
    }
}