package com.arch.example.ui

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.FloatingWindow
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.arch.example.R
import com.arch.example.base.BaseActivity
import com.arch.example.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun getActivityLayoutRedId(): Int = R.layout.activity_main

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(binding.toolbar)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        setupNavigationBars()
    }

    private fun setupNavigationBars() {
        // Google solution to hide back button on toolbar for fragments which don't need it
        // https://developer.android.com/guide/navigation/navigation-ui#appbarconfiguration
        val fragmentsWithBottomNavigation = setOf(R.id.topicsFragment, R.id.photosFragment)
        val fragmentsWithoutToolbar = emptySet<Int>() // remove if app has only screens with toolbar

        appBarConfiguration = AppBarConfiguration(
            fragmentsWithBottomNavigation,
            binding.drawerLayout
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.drawerNavigationView.setupWithNavController(navController)
        binding.bottomNavigationView.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            // don't change bars if a dialog fragment
            if (destination is FloatingWindow) {
                return@addOnDestinationChangedListener
            }
            // Google solution to hide navigation bars
            // https://developer.android.com/guide/navigation/navigation-ui#listen_for_navigation_events
            binding.toolbar.isVisible = destination.id !in fragmentsWithoutToolbar
            val allowBottomAndDrawerNavigation = destination.id in fragmentsWithBottomNavigation
            binding.bottomNavigationView.isVisible = allowBottomAndDrawerNavigation
            binding.drawerLayout.setDrawerLockMode(if (allowBottomAndDrawerNavigation) {
                DrawerLayout.LOCK_MODE_UNLOCKED
            } else {
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}